package com.example.somar;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SomarController {
    
    @GetMapping("/somar")
    public Somar somar(@RequestParam(value="a", defaultValue="0") int a, 
                       @RequestParam(value="b", defaultValue="0") int b){
        return new Somar(a, b);
    }

}
