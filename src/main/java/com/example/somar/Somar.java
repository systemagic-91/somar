package com.example.somar;

public class Somar {
    
    private int a;
    private int b;
    private int soma;
    
    public Somar(int a, int b){
        this.a = a;
        this.b = b;
        this.soma = a+b;
    }
    
    public int getA(){
        return this.a;
    }
    
    public int getB(){
        return this.b;
    }
    
    public int getSoma(){
        return this.soma;
    }

    @Override
    public String toString(){
        return "soma={a: "+a+",\nb: "+b+",\nsoma: "+soma+" }";
    }
}
