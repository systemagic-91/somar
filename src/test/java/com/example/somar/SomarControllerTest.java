package com.example.somar;

import static org.hamcrest.Matchers.equalTo;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@SpringBootTest
@AutoConfigureMockMvc
public class SomarControllerTest {
    
    @Autowired
    private MockMvc mvc;

    @Test
    public void testSomarControllerEmpty() throws Exception{

        mvc.perform(MockMvcRequestBuilders.get("/somar").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().string(equalTo("{\"a\": 0,\"b\": 0,\"soma\": 0}")));
    }

    @Test
    public void testSomarController() throws Exception{

        mvc.perform(MockMvcRequestBuilders.get("/somar/?a=10&b=10").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().string(equalTo("{\"a\": 10,\"b\": 10,\"soma\": 20}")));
    }    
}
