package com.example.somar;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SomarTest {
    
    @Test
    public void testGetSoma(){
        
        int expected = 12;
        int actual;

        Somar soma = new Somar(5,7);

        actual = soma.getSoma();

        assertEquals(expected, actual);
    }
}
